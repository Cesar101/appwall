Template.form.events({
    'submit form': function (event) {
        event.preventDefault();

        var imageFile = event.currentTarget.children[0].files[0];

        var name = event.currentTarget.children[1].value;

        var message = event.currentTarget.children[2].value;
        
        if (name == "") {
            Materialize.toast('Dont submit any thing empty', 4000) // 4000 is the duration of the toast
            
        }

        if (imageFile == undefined) {
            Materialize.toast('Dont submit any thing empty', 4000) // 4000 is the duration of the toast
        }

        if (message == "") {
            Materialize.toast('Dont submit any thing empty', 4000) // 4000 is the duration of the toast
        }

        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {

            } else {

                Collections.Images.insert(imageFile, function (error, fileObject) {
                    if (error) {

                    } else {
                        Collections.Post.insert({
                            name: name,
                            createdAt: new Date(),
                            message: message,
                            ImageID: fileObject._id
                        });
                        $('.grid').masonry('reloadItems');
                    }
                });
            }
        });
    }
});


Template.register.events({
    'submit form': function (event) {
        event.preventDefault();
        var emailVar = event.target.registerEmail.value;
        var passwordVar = event.target.registerPassword.value;
        Accounts.createUser({
            email: emailVar,
            password: passwordVar
        });
    }
});

Template.login.events({
    'submit form': function (event) {
        event.preventDefault();
        var emailVar = event.target.loginEmail.value;
        var passwordVar = event.target.loginPassword.value;
        Meteor.loginWithPassword(emailVar, passwordVar);
    }
});

Template.dashboard.events({
    'click .logout': function (event) {
        event.preventDefault();
        Meteor.logout();
    }
});