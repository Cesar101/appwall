"use strict";

Template.posts.helpers({
    postList: function () {
        return Collections.Post.find({}, { sort: {createdAt: -1}
        
        });
    
    },

    Image: function () {
        var image = Collections.Images.find({
            _id: this.ImageID
        }).fetch();

        return image[0].url();

    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true,
                gutter: 20
            });
        });
    }
});